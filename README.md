# Moving Figure

## What's this?
With this you are able to play animation of yourownhandbuildedwhatever figure and route.

## Techs
Developed using `raylib v.4.0` and `gcc 11.2.0`.

### Building on GNU/Linux
Download latest raylib and gcc using your package manager, e.g.:<br>
`pacman -S gcc raylib`<br>

Launch the `compile.sh`.

### Building on something else
Good luck.

## Controls
### Editing
Editing avaiable only in `building`, `routing` and `PIT` modes.<br>
`LMB` - place/drag point<br>
`RMB` - delete point<br>

### Animation settings
#### Switches
`r` - toggle animation repeating<br>
`a` - toggle figure rotation<br>
`z` - reverse figure rotation<br>
`s` - toggle route rotation<br>
`x` - reverse route rotation<br>

#### Speed
Min movement speed = 1px/s<br>
Max movement speed = 100px/s<br>
<br>
`[` - decrease figure movement speed by 1 px/s<br>
`]` - increase figure movement speed by 1 px/s<br>
`-` - decrease figure movement speed by 10 px/s<br>
`=` - increase figure movement speed by 10 px/s<br>

#### Angular frequency
Min angular frequency = 0.01rad/s<br>
Max angular frequency = 0.1rad/s<br>
<br>
`.` - decrease figure rotation angular frequency by 0.01 rad/s<br>
`/` - increase figure rotation angular frequency by 0.01 rad/s<br>
`'` - decrease route rotation angular frequency by 0.01 rad/s<br>
`\` - increase route rotation angular frequency by 0.01 rad/s<br>
<br>
### Modes
`q` - Enter figure building mode<br>
`w` - Enter route building mode<br>
`e` - Enter animation playing mode<br>
`t` - Enter "Point in triangle" mode<br>

