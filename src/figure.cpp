#include <algorithm>
#include <iterator>

#include "figure.hpp"

bool Figure::IsEmpty() const {
   return points.empty();
}

bool Figure::IsPolygon() const {
   return points.size() > 2;
}

bool Figure::IsTriangle() const {
   return points.size() == 3;
}

bool Figure::HavePoint(const Point &pt) const {
   return findPoint(pt) != points.cend();
}

Points::const_iterator Figure::HaveNearbyPoint(const Point &pt, int precision/*= 1*/) const {
   for (int x = -precision; x <= precision; ++x) {
      for (int y = -precision; y <= precision; ++y) {
         auto it = findPoint(pt + Point(x, y));
         if (it != points.cend())
            return it;
      }
   }

   return points.cend();
}

Points::iterator Figure::HaveNearbyPoint(const Point &pt, int precision/*= 1*/) {
   for (int x = -precision; x <= precision; ++x) {
      for (int y = -precision; y <= precision; ++y) {
         auto it = findPoint(pt + Point(x, y));
         if (it != points.end())
            return it;
      }
   }

   return points.end();
}

void Figure::AddPoint(const Point &pt) {
   points.push_back(pt);
}

void Figure::DeletePoint(const Point& pt) {
   auto it = std::find(points.cbegin(), points.cend(), pt);
   if (it != points.cend())
      points.erase(it);
}

const Points& Figure::GetPoints() const {
   return points;
}

std::size_t Figure::GetPointsCount() const {
   return points.size();
}

void Figure::Clear() {
   points.clear();
}

// Lazy ctrl-c / ctrl-v :p
Points::iterator Figure::begin() { return points.begin(); }
Points::iterator Figure::end()   { return points.end(); }
Points::iterator Figure::last() {
   if (IsEmpty()) return points.end();
   else return std::prev(points.end());
}

Points::const_iterator Figure::cbegin() const { return points.cbegin(); }
Points::const_iterator Figure::cend() const { return points.cend(); }
Points::const_iterator Figure::clast() const {
   if (IsEmpty()) return points.cend();
   else return std::prev(points.cend());
}

Points::const_iterator Figure::findPoint(const Point& pt) const {
   return std::find(points.cbegin(), points.cend(), pt);
}

Points::iterator Figure::findPoint(const Point& pt) {
   return std::find(points.begin(), points.end(), pt);
}
