#include <cmath>
#include <cstdio>
#include <stdlib.h>
#include <algorithm>
#include <iterator>
#include <raylib.h>
#include <iostream>
#include "app.hpp"

template <typename t_>
static inline void logvar(const char* varname, const t_& varval) {
   std::cout << varname << ": " << varval << ";\n";
}

//////////////////////////////////
// Colors
constexpr Color ColorBG = BLACK;
constexpr Color ColorFG = WHITE;
constexpr Color ColorNonPoly = RED;
constexpr Color ColorPoint = BLUE;
constexpr Color ColorHidden = GRAY;
constexpr Color ColorMoving = ColorFG;
constexpr Color ColorPIT = GREEN;
constexpr Color ColorNotPIT = RED;
//////////////////////////////////
// Other
constexpr int defaultFPS = 24;
constexpr float radiusPoint = 3.0f;
//////////////////////////////////

static void DrawLine(const Point& p1, const Point& p2, Color color) {
   DrawLine(p1.x, p1.y, p2.x, p2.y, color);
}

static void DrawCircle(const Point& center, float radius, Color color) {
   DrawCircle(center.x, center.y, radius, color);
}

App::App(int w, int h) : 
   widthWindow(w), heightWindow(h),
      applicationState(State::Initializing), 
         preanimState(State::Null),
            enableRepeating(false), enableRotation(false), reverseRotation(false),
               speedPlaying(20.0f), angularFreq(0.05f), currentAngle(0.0f),
                  routeRotation(false), reverseRouteRotation(false),
                     routeAngularFreq(0.05f), routeCurrentAngle(0.0f),
                        userPIT(-3, -3)
                        // I promised myself never do like this....
                        { }

int App::Execute() {
   InitWindow(widthWindow, heightWindow, "Moving Figure");
   changeState(State::Building);
   SetTargetFPS(defaultFPS);
   while (!WindowShouldClose()) {
      update();
      
      BeginDrawing();
      redraw();
      EndDrawing();
   }

   CloseWindow();
   return EXIT_SUCCESS;
}

void App::update() {
   handleMouse();
   handleKeyboard();
   if (applicationState == State::Playing)
      play();
   if (applicationState == State::PointInTriangle)
      updatePIT();
}

void App::preplay() {
   deltaPoints.clear();
   if (currStart == Route.cend())
      return;

   for (auto it : Polygon.GetPoints())
      deltaPoints.push_back(it - *currStart);
}

void App::play() {
   if (nextTarget == Route.cend()) {
      if (enableRepeating) {
         currStart = Route.cbegin();
         movingPoint = *Route.cbegin();
         nextTarget = std::next(Route.cbegin());
      }
      else {
         puts("APP: Animation is over.");
         changeState(preanimState);
         preanimState = State::Null;
         return;
      }
   }

   const auto target = *nextTarget;

   float timeDelta = GetFrameTime();
   float distToTarget = movingPoint.GetDistance(target);
   float distToPass = std::min(speedPlaying * timeDelta, distToTarget);
   float scale = distToTarget / distToPass;

   if (distToTarget == distToPass)
      nextTarget++;
   
   movingPoint += (target - movingPoint) / scale;
   if (enableRotation)
      currentAngle += reverseRotation ? -angularFreq : angularFreq;
   if (routeRotation)
      routeCurrentAngle += reverseRouteRotation ? -routeAngularFreq : routeAngularFreq;
}

void App::updatePIT() {
   Point::CrdType result[3];
   int i = 0;
   auto prevPoint = *std::prev(Polygon.cend());
   for (auto& point : Polygon) {
      result[i++] = userPIT.PerpDotProduct(prevPoint, point);
      prevPoint = point;
   }

   isPIT = (result[0] >= 0 && result[1] >= 0 && result[2] >= 0) ||
      (result[0] <= 0 && result[1] <= 0 && result[2] <= 0);
}

void App::redraw() {
   ClearBackground(ColorBG);
   if (applicationState != State::Playing) {
      redrawBuildingFigure();
      redrawRoute();
   }
   else
      redrawMovingFigure();

   if (applicationState == State::PointInTriangle) {
      static int shimmer = 0;
      Color color = ColorNotPIT;
      if (isPIT && shimmer)
         color = ColorPIT;
      DrawCircle(userPIT, radiusPoint, color);
      shimmer = (shimmer + 1) % 2;
   }

   Figure* object = getCurrentFocus();
   if (object) {
      Point ptMouse = getMousePoint();
      auto it = object->HaveNearbyPoint(ptMouse);
      if (it != object->cend())
         DrawCircle(*it, radiusPoint, ColorPoint);
   }
}

void App::redrawBuildingFigure() const {
   if (Polygon.GetPointsCount() == 1) {
      Color colorPoint = applicationState == State::Building ? ColorPoint : ColorHidden;
      DrawCircle(*(Polygon.cbegin()), radiusPoint, colorPoint);
   }
   else {
      Color color = Polygon.IsPolygon() ? ColorFG : ColorNonPoly;
      if (applicationState != State::Building) { color = ColorHidden; }
      Point prevPoint = *(Polygon.GetPoints().rbegin());
      for (const auto& pt : Polygon.GetPoints()) {
         DrawLine(pt, prevPoint, color);
         prevPoint = pt;
      }
   }
}

void App::redrawMovingFigure() const {
   Points currFigure;
   Point deltaRoute = movingPoint - getScreenCenter();
   for (const auto& delta : deltaPoints) {
      Point pt = delta.GetTurned(deltaRoute.GetTurned(getScreenCenter(),
       routeCurrentAngle), currentAngle); 
      currFigure.push_back(pt);
   }

   Point prevPoint = *(currFigure.rbegin());
   for (const auto& pt : currFigure) {
      DrawLine(pt, prevPoint, ColorMoving);
      prevPoint = pt;
   }
}

void App::redrawRoute() const {
   if (Route.GetPointsCount() == 1) {
      Color colorPoint = applicationState == State::Routing ? ColorPoint : ColorHidden;
      DrawCircle(*(Route.cbegin()), radiusPoint, colorPoint);
   }
   else if (!Route.IsEmpty()){
      Color color = applicationState == State::Routing ? ColorFG : ColorHidden;
      auto itCur = Route.cbegin();
      auto itNext = std::next(itCur, 1);
      while (itNext != Route.cend())
      {
         DrawLine(*itCur, *itNext, color);
         itCur++; itNext++;
      }
   }
}

void App::changeState(App::State state) {
   if (state == State::Initializing) {
      puts("Bad state switch!");
      return;  
   }

   if (state == State::Playing && (Route.IsEmpty() || Polygon.IsEmpty())) {
      printf("APP: Can't play the animation. Check if your figure or route has any points.\n");
      return;
   }
   else {
      currentAngle = 0.0f;
      routeCurrentAngle = 0.0f;
      currStart = Route.cbegin();
      movingPoint = *Route.begin();
      nextTarget = std::next(Route.cbegin());
      if (applicationState != State::Playing) {
         preanimState = applicationState;
         preplay();
      }
   }

   if (state == State::PointInTriangle && !Polygon.IsTriangle()) {
      printf("APP: Why do I should do that? Your figure is not a triangle!\n");
      return;
   }

   applicationState = state;
   printf("APP: Application is in %s mode.\n", getStateName(applicationState));
}

void App::toggleRepeating() {
   enableRepeating = !enableRepeating;
   printf("APP: Repeating is %s.\n", enableRepeating ? "enabled" : "disabled");
}

void App::toggleRotation() {
   enableRotation = !enableRotation;
   printf("APP: Rotation is %s.\n", enableRotation ? "enabled" : "disabled");
}

void App::toggleReverseRotation() {
   reverseRotation = !reverseRotation;
   printf("APP: Rotations is %s.\n", reverseRotation ? "reversed" : "back to normal");
}

void App::toggleRouteRotation() {
   routeRotation = !routeRotation;
   printf("APP: Route rotation is %s.\n", routeRotation ? "enabled" : "disabled");
}

void App::toggleReverseRouteRotation() {
   reverseRouteRotation = !reverseRouteRotation;
   printf("APP: Route rotations is %s.\n", reverseRouteRotation ? "reversed" : "back to normal");
}

void App::increaseSpeed(float val) {
   constexpr float speedCeil = 100.0f;
   speedPlaying = std::min(speedCeil, speedPlaying + std::abs(val));
   printf("APP: Speed has %s%fpx/s.\n",
      speedPlaying == speedCeil ? "reached it's limit at " : "increased to ",
         speedPlaying);
}

void App::decreaseSpeed(float val) {
   constexpr float speedFloor = 1.0f;
   speedPlaying = std::max(speedFloor, speedPlaying - std::abs(val));
   printf("APP: Speed has %s%fpx/s.\n",
      speedPlaying == speedFloor ? "reached it's miniumum at " : "decreased to ",
         speedPlaying);
}

void App::increaseFreq() { 
   constexpr float freqCeil = 0.1f;
   angularFreq = std::min(freqCeil, angularFreq + 0.01f);
   printf("APP: Figure's angular frequency has %s%frad/s.\n",
      freqCeil == angularFreq ? "reached it's limit at " : "increased to ",
         angularFreq);
}

void App::decreaseFreq() {
   constexpr float freqFloor = 0.01f;
   angularFreq = std::max(freqFloor, angularFreq - 0.01f);
   printf("APP: Figure's angular frequency has %s%frad/s.\n",
      freqFloor == angularFreq ? "reached minimum at " : "decreased to ",
         angularFreq);
}

void App::increaseRouteFreq() { 
   constexpr float freqCeil = 0.1f;
   routeAngularFreq = std::min(freqCeil, routeAngularFreq + 0.01f);
   printf("APP: Route's angular frequency has %s%frad/s.\n",
      freqCeil == routeAngularFreq ? "reached it's limit at " : "increased to ",
         routeAngularFreq);
}

void App::decreaseRouteFreq() {
   constexpr float freqFloor = 0.01f;
   routeAngularFreq = std::max(freqFloor, routeAngularFreq - 0.01f);
   printf("APP: Route's angular frequency has %s%frad/s.\n",
      freqFloor == routeAngularFreq ? "reached minimum at " : "decreased to ",
         routeAngularFreq);
}

Point App::getMousePoint() const {
   return Point(GetMouseX(), GetMouseY());
}

Figure* App::getCurrentFocus() {
   if (applicationState == State::Building)
      return &Polygon;
   else if (applicationState == State::Routing)
      return &Route;
   return nullptr;
}

void App::handleMouse() {
   static Point* pCaptured = nullptr;
   Figure* object = getCurrentFocus();
   if (object) {
      Point mousePoint = getMousePoint();
      if (pCaptured) *(pCaptured) = mousePoint;
      if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
         auto it = object->HaveNearbyPoint(mousePoint);
         if (it == object->end())
            object->AddPoint(mousePoint);
         else {
            pCaptured = &(*it);
         }
      }
      else if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
         pCaptured = nullptr;
      }
      else if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT)) {
         auto it = object->HaveNearbyPoint(mousePoint);
         if (it != object->cend())
            object->DeletePoint(*it);
      }
   }
   else if (applicationState == State::PointInTriangle) {
      if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
         userPIT = getMousePoint();
      else if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT))
         userPIT = Point(-3, -3);
   }
}

void App::handleKeyboard() {
   constexpr float smallDelta = 1.0f;
   constexpr float bigDelta = 10.0f;
   //// Mode changing ///////////////////////
   if (IsKeyPressed(KEY_Q))
      changeState(State::Building);
   else if (IsKeyPressed(KEY_W))
      changeState(State::Routing);
   else if (IsKeyPressed(KEY_E))
      changeState(State::Playing);
   else if (IsKeyPressed(KEY_T))
      changeState(State::PointInTriangle);
   
   //// Options //////////////////////////////
   else if (IsKeyPressed(KEY_R))
      toggleRepeating();
   else if (IsKeyPressed(KEY_A))
      toggleRotation();
   else if (IsKeyPressed(KEY_Z))
      toggleReverseRotation();
   else if (IsKeyPressed(KEY_S))
      toggleRouteRotation();
   else if (IsKeyPressed(KEY_X))
      toggleReverseRouteRotation();

   else if (IsKeyPressed(KEY_LEFT_BRACKET))
      decreaseSpeed(smallDelta);
   else if (IsKeyPressed(KEY_RIGHT_BRACKET))
      increaseSpeed(smallDelta);
   else if (IsKeyPressed(KEY_MINUS))
      decreaseSpeed(bigDelta);
   else if (IsKeyPressed(KEY_EQUAL))
      increaseSpeed(bigDelta);

   else if (IsKeyPressed(KEY_PERIOD))
      decreaseFreq();
   else if (IsKeyPressed(KEY_SLASH))
      increaseFreq();
   else if (IsKeyPressed(KEY_GRAVE))
      decreaseRouteFreq();
   else if (IsKeyPressed(KEY_BACKSLASH))
      increaseRouteFreq();
   
   //// Useful ///////////////////////////////
   else if (IsKeyPressed(KEY_C)) {
      if (applicationState == State::Building) {
         puts("APP: Clearing figure.");
         Polygon.Clear();
      }
      else if (applicationState == State::Routing) {
         puts("APP: Clearing the route.");
         Route.Clear();
         nextTarget = Route.cend();
         currStart = Route.cend();
      }
   }

   else if (IsKeyPressed(KEY_ESCAPE)) {
      puts("APP: Closing application.");
      CloseWindow();
   }
   ///////////////////////////////////////////
}

const char* App::getStateName(App::State state) const {
   switch (state) {
      case State::Initializing: return "Initializing";
      case State::Building: return "Building";
      case State::Routing: return "Routing";
      case State::Playing: return "Playing";
      case State::PointInTriangle: return "PIT";
      default: return "This is not supposed to happen...";
   }
}

Point App::getScreenCenter() const {
   return Point(static_cast <Point::CrdType> (widthWindow) / 2, 
      static_cast<Point::CrdType>(heightWindow) / 2);
}
