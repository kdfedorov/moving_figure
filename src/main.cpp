#include "app.hpp"

constexpr int widthScreen = 800;
constexpr int heightScreen = 600;

int main(void)
{
    App app(widthScreen, heightScreen);
    return app.Execute();
}
