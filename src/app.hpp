#include "figure.hpp"

// Application class
class App {
   //////////////////////////////////////////
      App() = delete;
      App(const App&) = delete;
      App& operator= (const App&) = delete;
   //////////////////////////////////////////

   // App states enumeration
   enum class State
   {
      Null,
      Initializing,
      Building,
      Routing,
      Playing,
      PointInTriangle
   };

   public:
      // Constructing app object
      App(int w, int h);
      // Executing application
      int Execute();

   private:
      // Updates application data
      void update();
      // Preplaying calculations
      void preplay();
      // Playing animation
      void play();
      // Calculates if user point is in triangle
      void updatePIT();

      // Changes application state
      void changeState(State state);

      // Enables/Disables playing repeating
      void toggleRepeating();
      // Enables/Disables figure rotation
      void toggleRotation();
      // Enables/Disables reverse rotation
      void toggleReverseRotation();
      // Enables/Disables route rotation
      void toggleRouteRotation();
      // Enables/Disables route reverse rotation
      void toggleReverseRouteRotation();
      
      // Increases speed of figure movement
      void increaseSpeed(float val);
      // Decreases ...
      void decreaseSpeed(float val);
      // Increases angular frequency of moving figure
      void increaseFreq();
      // Decreases ...
      void decreaseFreq();
      // Increases anglular frequency of route rotation
      void increaseRouteFreq();
      // Decreases ...
      void decreaseRouteFreq();

      // Gets mouse position
      Point getMousePoint() const;
      // Gets current focused figure
      Figure* getCurrentFocus();

      // Redraws window
      void redraw();
      // Redraws figure of Building/Routing mode
      void redrawBuildingFigure() const;
      // Redraws figure of Playing mode
      void redrawMovingFigure() const;
      // Redraws route
      void redrawRoute() const;

      // Handles mouse
      void handleMouse();
      // Handles keyboard
      void handleKeyboard();

      // Returns string with name of state
      const char* getStateName(App::State state) const;
      // Returns point of screen center
      Point getScreenCenter() const;
   private:
      //// Application state /////////////
      // Current application state
      State applicationState;
      // State before playing animation
      State preanimState;

      //// Window size ///////////////////
      int widthWindow;  // Window width
      int heightWindow; // Window height

      //// Entities //////////////////////
      Figure Polygon;   // Moving figure
      Figure Route;     // Route of moving figure
      Point userPIT;    // User point 
      bool isPIT;       // true, if user point is in triangle

      // Difference between start of route and moving figure
      Points deltaPoints;  

      //// Play options //////////////////
      // Figure settings //
      float speedPlaying;
      float angularFreq;
      float currentAngle;
      bool enableRotation;
      bool reverseRotation;

      // Route settings ///
      bool enableRepeating;
      bool routeRotation;
      bool reverseRouteRotation;
      float routeAngularFreq;
      float routeCurrentAngle;

      Point movingPoint;
      Points::const_iterator currStart;
      Points::const_iterator nextTarget;
      ////////////////////////////////////
};
