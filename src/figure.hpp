#include <cmath>
#include <cstdio>
#include <iostream>
#include <list>
#include <math.h>

template <typename t_>
t_ square(t_ arg) { return arg * arg; }

// Struct that contains point coordinates
template <typename t_>
struct PointTemplate {
   PointTemplate(t_ _x = 0, t_ _y = 0) : x(_x), y(_y) {}

   // Coordinates
   t_ x;
   t_ y;

   // Type used for coords
   using CrdType = t_;

   // Returns distance to point 'pt'
   t_ GetDistance(const PointTemplate<t_>& pt) const {
      PointTemplate<t_> delta(std::abs(pt.x - x), std::abs(pt.y - y));
      return std::sqrt(square(delta.x) + square(delta.y));
   }

   // Prints point info
   void Print(const char* name = "Point") const {
      if (name == nullptr)
         name = "Point";
      std::cout << name << ": " << "(" << x << ", " << y << ");\n";
   }

   // Compares two points
   bool operator==(const PointTemplate<t_>& pt) const
   { return x == pt.x && y == pt.y; }
   
   // Summing to points
   PointTemplate<t_> operator+(const PointTemplate<t_>& pt) const 
   { return PointTemplate<t_>(x + pt.x, y + pt.y); }

   // Adding 'pt' to this
   PointTemplate<t_> operator+=(const PointTemplate<t_>& pt) 
   { x += pt.x; y += pt.y; return *this; }

   // Substracts two points
   PointTemplate<t_> operator-(const PointTemplate<t_>& pt) const
   { return PointTemplate<t_>(x - pt.x, y - pt.y); }

   // Scalar multiplication
   template <typename targ_>
   PointTemplate<t_> operator*(targ_ arg) const
   { return Point(x * arg, y * arg); }

   // Scalar division
   template <typename targ_>
   PointTemplate<t_> operator/(targ_ arg) const
   { return PointTemplate<t_>(x / arg, y / arg);}

   // ;)
   PointTemplate<t_> GetTurned(const PointTemplate<t_>& origin, t_ angle) const {
      return origin + PointTemplate<t_>((x * std::cos(angle)) - (y * std::sin(angle)),
         (x * std::sin(angle)) + (y * std::cos(angle)));
   }

   t_ PerpDotProduct(const PointTemplate<t_>& pt1, const PointTemplate<t_>& pt2) const {
      return x * (pt2.y - pt1.y) + y * (pt1.x - pt2.x) + pt1.y * pt2.x - pt1.x * pt2.y;
   }
};

// Points, used in this program
using Point = PointTemplate<float>;
// Points container
using Points = std::list <Point>;

// Class of the figure, that app draws on screen
class Figure {
public:
   // Returns true, if figure have no points
   bool IsEmpty() const;
   // Returns true, if figure is a polygon
   bool IsPolygon() const;
   // Returns true, if figure is a triangle
   bool IsTriangle() const;
   // Returns true, if figure have input point
   bool HavePoint(const Point& pt) const;
   // Returns iterator, if figure contains point nearby
   Points::const_iterator HaveNearbyPoint(
      const Point& pt, int precision = 1) const;
   // ...
   Points::iterator HaveNearbyPoint(const Point& pt, int precision = 1);

   // Adds point to figure
   void AddPoint(const Point& pt);
   // Removes point from figure
   void DeletePoint(const Point& pt);
   // Returns read-only point vector
   const Points& GetPoints() const;
   // Get points count
   std::size_t GetPointsCount() const;
   // Clears figure
   void Clear();

   ///// Iterators /////
   Points::iterator begin();
   Points::iterator last();
   Points::iterator end();
   
   Points::const_iterator cbegin() const;
   Points::const_iterator clast() const;
   Points::const_iterator cend() const;
private:
   // Finds point in figure
   Points::const_iterator findPoint(const Point& pt) const;
   Points::iterator findPoint(const Point& pt);

private:
   Points points;
};
