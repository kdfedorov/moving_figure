# Target info
TARGET_DIR="./target"
EXEC_NAME="mf.x"
OUTPUT="${TARGET_DIR}/${EXEC_NAME}"

# Removing previously compiled program
if [ -f ${OUTPUT} ]; then
   rm -f ${OUTPUT}
fi
# Making target directory if it's not exists
if [ ! -d ${TARGET_DIR} ]; then
   mkdir ${TARGET_DIR}
fi

# Compiler flags
FLAGS="-lraylib -lGL -lm -lpthread -ldl -lrt -lX11 -g"
# Sources directory
SRC_DIR="./src"

# Source files list
SOURCES="
   ${SRC_DIR}/main.cpp 
   ${SRC_DIR}/app.cpp
   ${SRC_DIR}/figure.cpp
"

# Compiling
g++ ${SOURCES} ${FLAGS} -o ${OUTPUT}
